<?php

namespace App\Model;

use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\Models\Media;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Event extends Model implements HasMedia
{
    use HasMediaTrait,HasTranslations;
    protected $fillable = ['title','description'];
    public $translatable = ['title','description'];
}

<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Product extends Model implements HasMedia
{
    use HasTranslations,HasMediaTrait;
    
    public $translatable = ['name','description'];

    protected $fillable = [
        'rating','contact_number','youtube_id','price','product_category_id','promotion_price'
    ];
}

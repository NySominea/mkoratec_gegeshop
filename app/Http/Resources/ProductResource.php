<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $images = [];
        foreach($this->getMedia('images') as $temp){
            $images[] = $temp->getFullUrl();
        }
        return [
            'id' => $this->id,
            'name' => $this->getTranslation('name',getLanguage()),
            'has_promotion' => $this->has_promotion,
            'promotion_price' => $this->promotion_price,
            'rating' => $this->rating,
            'price' => $this->price,
            'description' => $this->getTranslation('description',getLanguage()),
            'video_id' => $this->youtube_id,
            'thumbnail' => $this->getFirstMedia('thumbnail')->getFullUrl(),
            'images' => $images,
            'contact_number' => $this->contact_number
        ];
    }
}

<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\TranslationLoader\LanguageLine;

class LanguageController extends Controller
{
    public function __construct(){
        $this->middleware('permission:view-language',   ['only' => ['index']]);
        $this->middleware('permission:language-modification',   ['only' => ['store','destroy']]);
    }
    public function index()
    {
        $languages = LanguageLine::all();
        return view('admin.language.index',compact('languages'));
    }

    public function store(Request $request)
    {   
        foreach($request->languages as $id => $row){
            $language = LanguageLine::where('id',$id)->first(); 
            if($language){
                $language->update([
                    'text' => $row,
                ]);
            }
        }
        return back()->withSuccess('You have succesfully updated the language setting.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Product;
use App\Model\ProductCategory;
use Exception;
use DB;

class ProductController extends Controller
{
    function index(){
        $products = Product::orderBy('id','desc')->paginate(10);
        return view('admin.product.index',compact('products'));
    }

    function create(){
        $categories = ProductCategory::pluck('name','id');
        return view('admin.product.add-update',compact('categories'));
    }

    function edit($id){
        $categories = ProductCategory::pluck('name','id');
        $product = Product::find($id);

        return view('admin.product.add-update',compact('product','categories'));
    }

    function store(Request $request){
        // dd($request->all());
        // $this->validate($request,[
        //     'name.*' => 'required|string|max:255',
        //     'productThumbnailDropzoneImage' => 'required',
        //     'productThumbnailDropzoneAction' => 'required',
        //     'productImagesDropzoneImage' => 'required',
        //     'productImagesDropzoneAction' => 'required',
        //     'price' => 'required',
        //     'rating' => 'required|min:1|max:5',
        //     'product_category_id' => 'required',
        //     'contact_number' => 'required'
        // ],[
        //     'name.*' => 'Name is required',
        // ]);

        DB::beginTransaction();

        try{
            $product = new Product();
            $product->setTranslations('name',$request->name);
            $product->setTranslations('description',$request->description);
            $product->fill($request->all());
            $product->save();

            saveModelSingleImage($product,$request->productThumbnailDropzoneImage,'thumbnail');
            saveModelMultiImage($product,$request->productImagesDropzoneImage,'images');

            if($request->push_notification === 'on'){
                pushNotification('New Product Released!', $product->getTranslation('name','en'));
            }

            DB::commit();

            return redirect()->route('admin.product.index')->withSuccess('Product added successfully!');
        }catch(Exception $ex){
            DB::rollback();
            dd($ex->getMessage());

            return back()->withErrors([
                $ex->getMessage()
            ]);
        }
    }

    function update(Request $request,$id){
        $this->validate($request,[
            'name.*' => 'required|string|max:255',
            'productThumbnailDropzoneAction' => 'required',
            'productImagesDropzoneAction' => 'required',
            'product_category_id' => 'required',
            'price' => 'required',
            'rating' => 'required|min:1|max:5',
            'contact_number' => 'required'
        ],[
            'name.*' => 'Name is required',
        ]);

        DB::beginTransaction();

        try{
            $product = Product::find($id);
            $product->setTranslations('name',$request->name);
            $product->setTranslations('description',$request->description);
            $product->fill($request->all());

            if($request->productThumbnailDropzoneImage)
                saveModelSingleImage($product,$request->productThumbnailDropzoneImage,'thumbnail');

            if($request->productImagesDropzoneImage)
                saveModelMultiImage($product,$request->productImagesDropzoneImage,'images');

            if($request->productImagesDropzoneDelete)
                deleteModelMultiImage($product,$request->productImagesDropzoneDelete,'images');

           
            $product->has_promotion = isset($request->promotion_price);

            $product->save();

            DB::commit();

            return redirect()->route('admin.product.index')->withSuccess('Product added successfully!');
        }catch(Exception $ex){
            DB::rollback();
            

            return back()->withErrors([
                $ex->getMessage()
            ]);
        }
    }

    function destroy($id){
        DB::beginTransaction();

        try{
            $product = Product::find($id);

            $product->delete();

            DB::commit();
            return response()->json([
                'success' => 'Product deleted successfully'
            ]);
        }catch(Exception $ex){
            DB::rollback();
            return back()->withErrors([ $ex->getMessage() ]);
        }
    }
}

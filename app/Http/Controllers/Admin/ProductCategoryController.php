<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\ProductCategory;
use DB;
use Exception;

class ProductCategoryController extends Controller
{
    public function index(){
        $categories = ProductCategory::orderBy('sort','ASC')->paginate(10);
        return view('admin.category.index',compact('categories'));
    }

    public function create(){
        return view('admin.category.add-update');
    }

    public function edit($id){
        $category = ProductCategory::find($id);

        return view('admin.category.add-update',compact('category'));
    }

    public function store(Request $request){
        $this->validate($request,[
            'name.*' => 'required|string|max:255',
            'categoryDropzoneImage' => 'required',
            'categoryDropzoneAction' => 'required'
        ],[
            'name.*' => 'Name is required'
        ]);

        DB::beginTransaction();
        try{
            $category = new ProductCategory();
            $category->setTranslations('name',$request->name);
            $category->setTranslations('description',$request->description);
            $category->save();
            saveModelSingleImage($category,$request->categoryDropzoneImage,'image');

            DB::commit();

            return redirect()->route('admin.product-category.index')
                ->withSuccess('Category created successfully');
        }catch(Exception $ex){
            DB::rollback();
            return back()->withErrors([ $ex->getMessage() ]);
        }
    }

    public function update(Request $request, $id){
        $this->validate($request,[
            'name.*' => 'required|string|max:255',
            'categoryDropzoneAction' => 'required'
        ],[
            'name.*' => 'Name is required'
        ]);
        DB::beginTransaction();
        try{
            $category = ProductCategory::find($id);
            $category->setTranslations('name',$request->name);
            $category->setTranslations('description',$request->description);
            $category->save();

            if($request->categoryDropzoneImage)
                saveModelSingleImage($category,$request->categoryDropzoneImage,'image');

            DB::commit();

            return redirect()->route('admin.product-category.index')
                ->withSuccess('Category updated successfully');
        }catch(Exception $ex){
            DB::rollback();
            return back()->withErrors([ $ex->getMessage() ]);
        }
    }

    public function destroy($id){
        DB::beginTransaction();

        try{
            $category = ProductCategory::find($id);

            $category->delete();

            DB::commit();
            return response()->json([
                'success' => 'Category deleted successfully'
            ]);
        }catch(Exception $ex){
            DB::rollback();
            return back()->withErrors([ $ex->getMessage() ]);
        }
    }

    public function setOrder(){
       DB::beginTransaction();
        try{
            foreach(request()->order_id as $index => $id){
                $category = ProductCategory::find($id);
                if($category){
                    $category->update(['sort' => $index + 1]);
                }
            }

            DB::commit();

            return response()->json(['success' => true]);
        }catch(Exception $ex){
            dd($ex);
            DB::rollback();
            return response()->json(['success' => false]);
        }
    }
}

<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Storage;
use File;

class ImageController extends Controller
{
    
    public function summernoteSaveImage(){
        $this->validate(request(),[
            'file' => 'image|mimes:jpg,jpeg,png'
        ]);

        $path = saveSummernoteImage(request()->file('file'));

        return response()->json([   
            'url' => url('/').'/'.$path
        ]);
    }

    public function dropzoneSaveTempImage(){
        if(request()->hasFile('file')){
            $path = saveTempImage(request()->file('file')); 
            return response()->json(['status' => true, 'path' => $path]);
        }
        return response()->json(['status' => false]);
    }

    public function dropzoneSaveMultiTempImage(){
        if(request()->hasFile('file')){
            foreach(request()->file('file') as $file){
                $path[] = saveTempImage($file); 
            }
            return response()->json(['status' => true, 'path' => implode(',',$path)]);
        }
        return response()->json(['status' => false]);
    }

    public function getImageList(){ 
        $id = request()->id;
        $object = request()->object;
        $object = $object::findOrFail($id);
        $images = [];
        if(count($object->getMedia(request()->collection)) > 0){
            foreach($object->getMedia(request()->collection) as $image){
                $images[] = ['name' => $image->file_name, 'size' => $image->size, 'url' => $image->getUrl()];
            }
            return response()->json(['success' => true, 'images' => $images]);
        }else{
            return response()->json(['success' => false]);
        }


    }
}

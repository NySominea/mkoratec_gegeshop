<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Product;
use App\Model\ProductCategory;
use App\Http\Resources\ProductResource;
use App\Http\Resources\ProductCategoryResource;
use DB;

class HomeController extends Controller
{

    function getSpecialOfferProducts(){
        
    }

    function getPopularProducts(){
        

        return response()->json([
            'products' => ProductResource::collection(
                Product::whereProductCategoryId(
                    $product_category->id
                )
            )
        ]);
    }

    function getProductsByCategory(){
        return response()->json([
            'products' => ProductResource::collection(
                Product::whereProductCategoryId(
                    request()->get('category_id')
                )->paginate(10)
            )
        ]);
    }

    function getCategories(){
        return response()->json([
            'categories' => ProductCategoryResource::collection(ProductCategory::all())
        ]);
    }

    function specialOffers(){
        $special_offers = Product::whereHasPromotion(true)
                            ->paginate(15);

        return response()->json([
            'products' => ProductResource::collection($special_offers)
        ]);
    }

    function searchProducts(){
        $keyword = request()->get('keyword');

        if(!$keyword)
        return response()->json([
            'products' => []
        ]);

        $page = request()->get('page',1);
        $offset = 15 * ($page - 1);
        $ids = DB::select(DB::raw("SELECT id from products where name like '%$keyword%' limit 20 offset $offset"));

        $temp = [];
        foreach($ids as $id){
            $temp[] = $id->id;
        }
        
        $products = Product::whereIn('id',$temp)->get();

        return response()->json([
            'products' => ProductResource::collection($products)
        ]);
    }

    function home(){
        $product_category = ProductCategory::where('name','like',"%Special Offer%")->first();
        $special_offers = Product::whereProductCategoryId($product_category)->take(10)->get();

        $new_products = Product::orderBy('id','desc')->take(10)->get();

        $product_category = ProductCategory::where('name','like',"%Popular Product%")->first();
        $popular_products = Product::whereProductCategoryId($product_category)->take(10)->get();

        return response()->json([
            'special_offers' => ProductResource::collection($special_offers),
            'new_products' => ProductResource::collection($new_products),
            'popular_products' => ProductResource::collection($popular_products)
        ]);
    }
}

<?php
use App\Model\Setting;
use KCE\OneSignal\Facades\OneSignalClient;

function settings(){
    $companySettings = Setting::all()->keyBy('key');
    return $companySettings;
}

function slugify($text)
{
  // replace non letter or digits by -
  $text = preg_replace('~[^\pL\d]+~u', '-', $text);

  // transliterate
  $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

  // remove unwanted characters
  $text = preg_replace('~[^-\w]+~', '', $text);

  // trim
  $text = trim($text, '-');

  // remove duplicate -
  $text = preg_replace('~-+~', '-', $text);

  // lowercase
  $text = strtolower($text);

  if (empty($text)) {
    return 'n-a';
  }

  return $text;
}

function saveModelSingleImage($model,$path,$collection){
    if(isset($path) && !empty($path)){
        $model->clearMediaCollection($collection); 
        $model->addMedia($path)
             ->toMediaCollection($collection);
    }
}

function saveModelMultiImage($model,$path,$collection){
    if(isset($path) && !empty($path)){ 
        $imageNameList = explode( ',', $path);
        foreach($imageNameList as $image){
            if(File::exists($image)){
                $model->addMedia($image)
                        ->toMediaCollection($collection);
            }
        }
    }

}

function deleteModelMultiImage($model,$nameImageList = [], $collection){
    $images = $model->getMedia($collection)
        ->whereIn('file_name',explode(',',$nameImageList));
    $images->each(function($image){
        $image->delete();
    });

}

function saveTempImage($image){
    $filename = time().'-'.$image->getClientOriginalName();
    $destination = 'app-assets/images/temp';
    
    $image->move(public_path($destination), $filename);
    $path = $destination.'/'.$filename;

    return $path;
}

function saveSummernoteImage($image){
    $filename =time().'-'.$image->getClientOriginalExtension();
    $destination = 'app-assets/images/summernote';
    
    $image->move(public_path($destination), $filename);
    $path = $destination.'/'.$filename;

    return $path;
}

function pushNotification($title,$msg){
    OneSignalClient::setTitle($title)->sendToAll($msg);
}

function getLanguage(){
    return request()->header('Accept-Language') ? : 'en';
}

function permissions(){
    return [
        'product' => [
            'module' => 'Product',
            'permissions' => [
                ['text' => 'View Product', 'value' => 'view-product'],
                ['text' => 'Add New Product', 'value' => 'add-new-product'],
                ['text' => 'Modify Product', 'value' => 'modify-product'],
            ]
        ],
        'product-category' => [
            'module' => 'Product Category',
            'permissions' => [
                ['text' => 'View Product Category', 'value' => 'view-product-category'],
                ['text' => 'Add New Category', 'value' => 'add-new-category'],
                ['text' => 'Modify Category', 'value' => 'modify-category'],
            ]
        ],
        'user' => [
            'module' => 'User',
            'permissions' => [
                ['text' => 'View User', 'value' => 'view-user'],
                ['text' => 'User Modification', 'value' => 'user-modification'],
                ['text' => 'Add New User', 'value' => 'add-new-user'],
                ['text' => 'View Role', 'value' => 'view-role'],
                ['text' => 'Role Modification', 'value' => 'role-modification'],
                ['text' => 'Add New Role', 'value' => 'add-new-role'],
            ]
        ],
        'setting' => [
            'module' => 'Setting',
            'permissions' => [
                ['text' => 'View Setting', 'value' => 'view-setting'],
                ['text' => 'Setting Modification', 'value' => 'setting-modification']
            ]
            ],
        'language' => [
            'module' => 'Language',
            'permissions' => [
                ['text' => 'View Language', 'value' => 'view-language'],
                ['text' => 'Language Modification', 'value' => 'language-modification']
            ]
        ]
    ];
}

function settingKeys(){
    return [
        'logo',
        'animated_logo',
        'site_url',
        'site_name',
        'phone',
        'address',
        'email',
        'copy_right',

        'member_banner',
        'about_us_banner',
        'register_banner',
        'gallery_banner',
        'event_banner',
        'footer_banner',

        'youtube',
        'facebook',
        'linkedin',
        'instagram',

        'seo_keyword',
        'seo_description',

        'our_story_description',
        'register_description',

        'bank_account_name',
        'bank_account_number'
    ];
}

function frontendLanguage(){
    return [
        'home_menu' => ['en' => 'Home'],
        'donate_menu' => ['en' => 'Donate'],
        'event_menu' => ['en' => 'Event'],
        'gallery_menu' => ['en' => 'Gallery'],
        'register_menu' => ['en' => 'Register'],
        'about_menu' => ['en' => 'About Us'],
        'view_more_button' => ['en' => 'View More'],
        'learn_more_button' => ['en' => 'Learn More'],
        'register_button' => ['en' => 'Register'],
        'discard_button' => ['en' => 'Discard'],
        'select_button' => ['en' => 'Select'],
        'vision_label' => ['en' => 'Our Visions'],
        'mission_label' => ['en' => 'Our Missions'],
        'core_value_label' => ['en' => 'Our Core Values'],
        'event_label' => ['en' => 'Our Events'],
        'latest_event_label' => ['en' => 'Latest Events'],
        'gallery_label' => ['en' => 'Our Galleries'],
        'partner_label' => ['en' => 'Our Partners'],
        'story_label' => ['en' => 'Our Story'],
        'member_label' => ['en' => 'Our Members'],
        'member_registration_label' => ['en' => 'Member Registration'],
        'short_biography_label' => ['en' => 'Short Biography'],
        'today_view_label' => ['en' => 'Today View'],
        'account_name_label' => ['en' => 'Account Name'],
        'account_number_label' => ['en' => 'Account Number'],
        'no_content_label' => ['en' => 'No available content'],
        'all_album_label' => ['en' => 'All Photo Album'],
        'all_album_in_province' => ['en' => 'All Photo Album in :province'],
        'all_photo_of_location_in_province' => ['en' => 'All Photo of :location in :province'],
        'province_placeholder' => ['en' => 'Please select a province'],
        'term_condition_label' => ['en' => 'Term & Condition'],
        'kh_name_label' => ['en' => 'Khmer Name'],
        'en_name_label' => ['en' => 'English Name'],
        'gender_label' => ['en' => 'Gender'],
        'male_label' => ['en' => 'Male'],
        'female_label' => ['en' => 'Female'],
        'age_label' => ['en' => 'Age'],
        'id_number_label' => ['en' => 'ID Number'],
        'address_label' => ['en' => 'Address'],
        'photo_4x6' => ['en' => 'Photo 4x6'],
        'education_label' => ['en' => 'Education'],
        'language_label' => ['en' => 'Language'],
        'career_position_label' => ['en' => 'Career Position'],
        'phone_label' => ['en' => 'Phone Number'],
        'choose_photo_label' => ['en' => 'Choose Photo'],
        'email_label' => ['en' => 'Email'],
        'facebook_label' => ['en' => 'Facebook'],
        'phone_label' => ['en' => 'Phone Number'],
        'kh_name_placeholder' => ['en' => 'Please enter your Khmer name'],
        'en_name_placeholder' => ['en' => 'Please enter your English name'],
        'age_placeholder' => ['en' => 'Please enter your age'],
        'id_number_placeholder' => ['en' => 'Please enter your ID number'],
        'address_placeholder' => ['en' => 'Please enter your address'],
        'education_placeholder' => ['en' => 'Please select your education'],
        'language_placeholder' => ['en' => 'Please select your languages'],
        'career_position_placeholder' => ['en' => 'Please enter your career position'],
        'phone_placeholder' => ['en' => 'Please enter your phone number'],
        'email_placeholder' => ['en' => 'Please enter your email address'],
        'facebook_placeholder' => ['en' => 'Please enter your Facebook name'],
        'optional_label' => ['en' => 'Optional'],
        'against_humanity_sentence' => ['en' => 'Are you engaging in any locally or internationally movements or activities against humanity?'],
        'yes_against_humanity_label' => ['en' => 'Yes, I am'],
        'no_against_humanity_label' => ['en' => 'No, I am not'],
        'register_condition_first_sentence' => ['en' => 'I guarantee and certify that I do not take on the role of an Associated Member or an Act to serve any political party'],
        'register_condition_second_sentence' => ['en' => 'I guarantee and certify that I do not take on the role of an Associated Member or an Act to serve any political party'],
        'kh_name_field_validation' => ['en' => 'The Khmer name field is required'],
        'en_name_field_validation' => ['en' => 'The English name field is required'],
        'age_field_validation' => ['en' => 'The age field is required'],
        'email_field_validation' => ['en' => 'The email field is required'],
        'phone_field_validation' => ['en' => 'The phone field is required'],
        'address_field_validation' => ['en' => 'The address field is required'],
        'education_field_validation' => ['en' => 'The education field is required'],
        'photo_field_validation' => ['en' => 'The photo field is required'],
        'id_number_field_validation' => ['en' => 'The ID number field is required'],
        'language_field_validation' => ['en' => 'The language field is required'],
        'career_position_field_validation' => ['en' => 'The career position field is required'],
        'fill_information_below_validation' => ['en' => 'Please fill all information in the form below.'],
        'agree_condition_below_validation' => ['en' => 'Please agree the term condition below.'],
    ];
}

function educationLevel(){
    return [
        'Doctoal\'s Degree',
        'Master\'s Degree',
        'Bachelor\'s Degree',
        'Association\'s Degree',
        'Secondary Education',
        'Primary Education',
    ];
}
function languageList(){
    return [
        'Khmer',
        'English',
        'Chinese',
        'French',
        'Thai',
        'Vietnamese'
    ];
}
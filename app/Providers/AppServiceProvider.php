<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use App\Model\ProjectCategory;
use View;
use App;
use Spatie\Analytics\Period;
use Analytics;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        // view()->composer('*', function ($view) {
        //     $view->with(['settings' => settings(), 'visitors' => Analytics::fetchTotalVisitorsAndPageViews(Period::days(2))->sum('visitors')]);
        // });
    }
}

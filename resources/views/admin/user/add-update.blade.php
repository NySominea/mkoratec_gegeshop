@extends('admin.layouts.master')

@section('content')
<div class="row">
    <div class="col-md-12">            
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">{{ isset($user) ? 'Update' : 'Add' }} Administrator users</h4>
            </div>

            @if(isset($user))
            {{ Form::model($user,['route' => ['admin.users.update',$user->id], 'method' => 'PUT']) }}
            @else
            {{ Form::open(['route' => 'admin.users.store', 'method' => 'POST']) }}
            @endif
            @csrf
            <div class="card-content">
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-6 order-lg-1 mb-md-2">
                            <fieldset class="form-group">
                                <label>Profile Image (Aspec Ratio 1:1)</label>
                                @component('admin.common.single_dropzone',['id' => "profileDropzone",'object' => isset($user) ? $user : null, 'width' => 150, 'height' => 150])@endcomponent
                            </fieldset>
                        </div>
                        <div class="col-md-6 order-lg-21 mb-md-2">
                            <fieldset class="form-group">
                                <label for="">Admininstrator Name</label>
                                {!! Form::text('name',null,['class' => 'form-control', 'placeholder' => 'Enter your name']) !!}
                                @component('admin.common.error_helper_text',['key' => "name"])@endcomponent
                            </fieldset>
                            <fieldset class="form-group">
                                <label for="">Email</label>
                                {!! Form::email('email',null,['class' => 'form-control', 'placeholder' => 'Enter email']) !!}
                                @component('admin.common.error_helper_text',['key' => "email"])@endcomponent
                            </fieldset>
                            <fieldset class="form-group">
                                <label for="">Password</label>
                                {!! Form::password('password',['class' => 'form-control', 'placeholder' => 'Please enter more than 6 characters']) !!}
                                @component('admin.common.error_helper_text',['key' => "password"])@endcomponent
                            </fieldset>
                            <fieldset class="form-group">
                                <label for="">Password Confirmation</label>
                                {!! Form::password('password_confirmation',['class' => 'form-control', 'placeholder' => 'Please enter more than 6 characters']) !!}
                                @component('admin.common.error_helper_text',['key' => "password_confirmation"])@endcomponent
                            </fieldset>
                            <fieldset class="form-group">
                                <label for="">Role Authority</label>
                                {!! Form::select("role_id",$roles, isset($user,$user->roles[0]) ? $user->roles[0]->id : null,['class' => 'custom-select block']) !!}
                                @component('admin.common.error_helper_text',['key' => "role_id"])@endcomponent
                            </fieldset>
                        </div>
                    </div>
                </div>
                
                <div class="card-footer">
                    <a href="{{route('admin.users.index')}}" class="btn grey btn-outline-secondary">Discard</a>
                    <button type="submit" class="btn btn-outline-primary">Save changes</button>
                </div>
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
@endsection


@extends('admin.layouts.master')

@section('page-css')
<link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/vendors.min.css">
<link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/forms/icheck/icheck.css">
<link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/forms/icheck/custom.css">
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">            
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">{{ isset($role) ? 'Update' : 'Add' }} Administrator Roles</h4>
            </div>

            @if(isset($role))
            {{ Form::model($role,['route' => ['admin.roles.update',$role->id], 'method' => 'PUT']) }}
            @else
            {{ Form::open(['route' => 'admin.roles.store', 'method' => 'POST']) }}
            @endif
            @csrf
            <div class="card-content">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <fieldset class="form-group">
                                <label for="basicInput">Role Name</label>
                                {!! Form::text('name',null,['class' => 'form-control', 'placeholder' => 'Enter role name']) !!}
                                @component('admin.common.error_helper_text',['key' => "name"])@endcomponent
                            </fieldset>
                            <fieldset class="form-group">
                                <label for="basicInput">Permissions</label>
                                @foreach(permissions() as $m => $module)
                                    <div class="row skin skin-square mb-2">
                                        <div class="col-12">
                                            <fieldset>
                                                {{Form::checkbox("permissions[$m][]",'0',isset($modules) && in_array($m,$modules)?true:false,["class" => "module"])}}
                                                <label class="strong"><strong>{{$module['module']}}</strong></label>
                                            </fieldset>
                                        </div>
                                        <div class="col-12 permission-list">
                                            @foreach($module['permissions'] as $p)
                                                <div class="d-inline-block mr-2">
                                                    <fieldset>
                                                        {{Form::checkbox("permissions[$m][]",$p['value'],isset($role) && in_array($p['value'],$role->permissions->pluck('name')->toArray())?true:false,["class" => "permission"])}}
                                                        <label>{{ $p['text'] }}</label>
                                                    </fieldset>
                                                </div>
                                            @endforeach
                                        </div>  
                                    </div>
                                @endforeach
                            </fieldset>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <a href="{{route('admin.roles.index')}}" class="btn grey btn-outline-secondary">Discard</a>
                    <button type="submit" class="btn btn-outline-primary">Save changes</button>
                </div>
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
@endsection

@section('page-script')
<script src="/app-assets/vendors/js/forms/icheck/icheck.min.js"></script>
<script src="/app-assets/js/scripts/forms/checkbox-radio.js"></script>
<script>
    $(document).ready(function(){
        $('.skin-square input').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });
        // Set checkbox on Role Set
        if($('.module').length > 0){
            $('.module').parent().find('.iCheck-helper').click(function(){
                if($(this).parent().find('input').is(':checked')){
                    $(this).parents('.skin-square').find('.permission').each(function(){
                        $(this).parent().addClass('checked');
                        $(this).prop('checked', true);
                    })
                }else{
                    $(this).parents('.skin-square').find('.permission').each(function(){
                        $(this).parent().removeClass('checked');
                        $(this).prop('checked', false);
                    })
                }
            })
        }
        if($('.permission').length > 0){
            $('.permission').parent().find('.iCheck-helper').click(function(){
                var hasOnePermission = false;
                $(this).parents('.skin-square').find('.module').prop('checked', true);
                $(this).parents('.skin-square').find('.module').parent().addClass('checked');
                if($(this).parents('.skin-square').find('.module').is(':checked')){ 
                    $(this).parents('.skin-square').find('.module').prop('checked', true);
                    $(this).parents('.skin-square').find('.module').parent().addClass('checked');
                }
                $(this).parents('.permission-list').find('.permission').each(function(){
                    if($(this).is(':checked')){
                        hasOnePermission = true;
                    }
                })
                if(!hasOnePermission){
                    $(this).parents('.skin-square').find('.module').prop('checked', false);
                $(this).parents('.skin-square').find('.module').parent().removeClass('checked');
                }

            });
        }
        // End checkbox on Role Set
    })
</script>
@endsection
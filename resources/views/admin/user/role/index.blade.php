@extends('admin.layouts.master')

@section('content')
<div class="row">
    <div class="col-md-12">            
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Administrator Roles</h4>
                @can('add-new-role')
                <div class="heading-elements">
                    <a href="{{route('admin.roles.create')}}" class="btn btn-sm btn-icon btn-success">
                        <i class="ft-plus white"></i> Add New
                    </a>
                </div>
                @endcan
            </div>
            <div class="card-content">
                <div class="card-body">
                    @include('admin.includes.success-msg')
                    @include('admin.includes.error-msg')
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead class="thead-light">
                                <th>#</th>
                                <th>Role</th>
                                <th>Action</th>
                            </thead>
                            <tbody>
                                <tr>
                                    @if(isset($roles) && $roles->count() > 0) 
                                        @foreach($roles as $key => $row)
                                        <tr>
                                            <td>{{($roles->perPage() * ($roles->currentPage() - 1)) + $key + 1}}</td>
                                            <td>{{ $row->name }}</td>
                                            <td class="group-btn-action">
                                                @canany(['role-modification'])
                                                    <div class="btn-group btn-group-sm" role="group" aria-label="Basic example">
                                                        <a href="{{ route('admin.roles.edit',$row->id) }}" class="btn btn-outline-warning"><i class="ft-edit-3"></i> Edit</a>
                                                        <button type="button" class="btn btn-outline-danger delete" data-route="{{route('admin.roles.destroy',$row->id)}}"><i class="ft-trash-2"></i> Delete</button>
                                                    </div>
                                                @endcanany
                                            </td>
                                        </tr>
                                        @endforeach
                                    @else 
                                        <tr><td colspan="3">No Data</td></tr>
                                    @endif
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            @if(isset($roles) && $roles->count() > 0)
            <div class="card-footer">
                <div class="mb-2">
                    {!! $roles->appends(Input::except('page'))->render() !!}
                </div>
                <div>
                    Showing {{$roles->firstItem()}} to {{$roles->lastItem()}}
                    of  {{$roles->total()}} entries
                </div>
            </div>
            @endif
        </div>
    </div>
</div>
@endsection

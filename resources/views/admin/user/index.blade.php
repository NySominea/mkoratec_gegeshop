@extends('admin.layouts.master')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Administrators</h4>
                <div class="heading-elements">
                    @can('add-new-user')
                    <a href="{{route('admin.users.create')}}" class="btn btn-sm btn-icon btn-success">
                        <i class="ft-plus white"></i> Add New
                    </a>
                    @endcan
                </div>
            </div>
            <div class="card-content">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead class="bg-primary">
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Role</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    @if(isset($users) && $users->count() > 0) 
                                        @foreach($users as $key => $row)
                                        @php $image = $row->getMedia('images')->first() @endphp
                                        <tr style="vertical-align:middle">
                                            <td>{{($users->perPage() * ($users->currentPage() - 1)) + $key + 1}}</td>
                                            @if($image)
                                                <td> <img class="rounded-circle" width="35px;" src="{{$image->geturl()}}"> {{ $row->name }}</td>
                                            @else
                                                <td>{{ $row->name }}</td>
                                            @endif
                                            <td>{{ $row->email }}</td>
                                            <td>{{ $row->roles && isset($row->roles[0]) ? $row->roles[0]->name : '-' }}</td>
                                            <td class="group-btn-action">
                                                @canany(['user-modification'])
                                                    <div class="btn-group btn-group-sm" aria-label="Basic example">
                                                        <a href="{{ route('admin.users.edit',$row->id) }}" class="btn btn-outline-warning"><i class="ft-edit-3"></i> Edit</a>
                                                        <button type="button" class="btn btn-outline-danger delete" data-route="{{route('admin.users.destroy',$row->id)}}"><i class="ft-trash-2"></i> Delete</button>
                                                    </div>
                                                @endcanany
                                            </td>
                                        </tr>
                                        @endforeach
                                    @else 
                                        <tr><td colspan="3">No Data</td></tr>
                                    @endif
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            @if(isset($users) && $users->count() > 0)
            <div class="card-footer">
                <div class="mb-2">
                    {!! $users->appends(Input::except('page'))->render() !!}
                </div>
                <div>
                    Showing {{$users->firstItem()}} to {{$users->lastItem()}}
                    of  {{$users->total()}} entries
                </div>
            </div>
            @endif
        </div>
    </div>
</div>
@endsection

@section('page-script')

<script>
</script>
@endsection
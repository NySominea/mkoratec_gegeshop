@extends('admin.layouts.master')
@section('content')
@include( 
    'admin.common.success_alert',['key' => 'success']
)
<div class="row">
    <div class="col-md-12">            
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Products</h4>
                <div class="heading-elements">
                    @can('add-new-category')
                    <a href="{{ route('admin.product.create') }}" class="btn btn-sm btn-icon btn-success">
                        <i class="ft-plus white"></i> Add New
                    </a>
                    @endcan
                </div>
            </div>
            <div class="card-content">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead class="bg-primary">
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Description</th>
                                    <th>Rating</th>
                                    <th>Price</th>
                                    <th>Image</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($products as $row)
                                    <tr>
                                        <td>{{ request()->get('page',0) * 10 + 1 }}</td>
                                        <td>{{ $row->getTranslation('name','en') }}</td>
                                        <td>{{ $row->getTranslation('description','en') }}</td>
                                        <td>
                                            <button type="button" class="btn btn-warning"><i class="fa fa-star"></i> {{ $row->rating }}</button>
                                        </td>
                                        <td>{{ $row->price }} $</td>
                                        <td>
                                            <img src="{{ $row->getFirstMedia('thumbnail') ? $row->getFirstMedia('thumbnail')->getUrl() : ''}}"
                                                class="rounded" width="30" height="30"/>
                                        </td>
                                        <td>
                                            @canany(['modify-category'])
                                                <div class="btn-group btn-group-sm" aria-label="Basic example">
                                                    <a href="{{ route('admin.product.edit',$row->id) }}" class="btn btn-outline-warning"><i class="ft-edit-3"></i> Edit</a>
                                                    <button type="button" class="btn btn-outline-danger delete" data-route="{{route('admin.product.destroy',$row->id)}}"><i class="ft-trash-2"></i> Delete</button>
                                                </div>
                                            @endcanany
                                        </td>
                                    </tr>
                                @empty
                                    <tr><td colspan="3">No Data</td></tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

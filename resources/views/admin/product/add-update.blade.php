@extends('admin.layouts.master')

@section('page-css')
<link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/vendors.min.css">
<link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/forms/icheck/icheck.css">
<link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/forms/icheck/custom.css">
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">            
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">{{ isset($product) ? 'Update' : 'Add' }} Product</h4>
            </div>

            @if(isset($product))
            {{ Form::model($product,['route' => ['admin.product.update',$product->id], 'method' => 'PUT']) }}
            @else
            {{ Form::open(['route' => 'admin.product.store', 'method' => 'POST']) }}
            @endif
            @csrf
            <div class="card-content">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <fieldset class="form-group">
                                <label>Product Thumbnail (Aspec Ratio 1:1) <span class="text-danger">*</span></label>
                                @component('admin.common.single_dropzone',['id' => "productThumbnailDropzone",'object' => isset($product) ? $product : null, 'width' => 375, 'height' => 150,'collection_key' => 'thumbnail'])@endcomponent
                            </fieldset>
                            <fieldset class="form-group">
                                <label>Product Preview Images (Aspec Ratio 1:1 or 4:3) <span class="text-danger">*</span></label>
                                @component('admin.common.multi_dropzone',['id' => "productImagesDropzone",'object' => isset($product) ? $product : null, 'width' => 100, 'height' => 100,'collection_key' => 'images'])@endcomponent
                            </fieldset>
                            
                            @if(isset($product))
                                {!! Form::hidden('has_promotion',$product->has_promotion,['id' => 'has-promotion']) !!}
                                <fieldset class="form-group">
                                    <button id="btn-promotion" type="button" class="btn btn-danger btn-min-width mr-1 mb-1"><i class="fa fa-percent"></i> Click to add discount</button>
                                </fieldset>
                                <div class="row" id="div-promotion">
                                    <div class="col-md-12"> 
                                        <fieldset class="form-group">
                                            <label for="basicInput">Promotion Price</label>
                                            {!! Form::number('promotion_price',null,['class' => 'form-control', 'placeholder' => 'Enter promotion price','id' => 'txt_promotion_price','step' => ".01"]) !!}
                                            @component('admin.common.error_helper_text',['key' => "promotion_price"])@endcomponent
                                        </fieldset>
                                    </div>
                                </div>
                                
                            @endif
                        </div>
                        <div class="col-md-6">
                            <ul class="nav nav-tabs nav-top-border no-hover-bg" role="tablist">
                                @foreach(['en','kh','zh'] as $key => $lang)
                                <li class="nav-item">
                                    <a class="nav-link {{ $key ==0 ? 'active' : '' }}" id="base-{{$lang}}" data-toggle="tab" aria-controls="{{$lang}}" href="#{{$lang}}" role="tab" aria-selected="true">
                                        <img width="30px" src="{{asset('client/images/'.$lang.'.jpeg')}}" alt="">
                                    </a>
                                </li>
                                @endforeach
                            </ul>
                            <div class="tab-content px-1 pt-1">
                                @foreach(['en','kh','zh'] as $key => $lang)
                                    <div class="tab-pane {{ $key == 0 ? 'active' : ''}}" id="{{$lang}}" role="tabpanel" aria-labelledby="base-{{$lang}}">
                                        <fieldset class="form-group">
                                            <label for="basicInput">Name <img width="20px" src="{{asset('client/images/'.$lang.'.jpeg')}}" alt=""></label>
                                            {!! Form::text('name['.$lang.']',isset($product) ? $product->getTranslation('name',$lang) : null,['class' => 'form-control', 'placeholder' => 'Enter product name']) !!}
                                            @component('admin.common.error_helper_text',['key' => "name.$lang"])@endcomponent
                                        </fieldset>
                                        <fieldset class="form-group">
                                            <label for="basicInput">Description <img width="20px" src="{{asset('client/images/'.$lang.'.jpeg')}}" alt=""></label>
                                            {!! Form::textarea('description['.$lang.']',isset($product) ? $product->getTranslation('description',$lang) : null,['class' => 'form-control', 'placeholder' => 'Enter product description','rows' => '4']) !!}
                                            @component('admin.common.error_helper_text',['key' => "description"])@endcomponent
                                        </fieldset>
                                    </div>
                                @endforeach
                            </div>
                            <fieldset class="form-group">
                                <label for="basicInput">Category</label>
                                {!! Form::select('product_category_id',$categories,null,['class' => 'form-control', 'placeholder' => 'Choose category']) !!}
                                @component('admin.common.error_helper_text',['key' => "product_category_id"])@endcomponent
                            </fieldset>
                            <fieldset class="form-group">
                                <label for="basicInput">Price ($)</label>
                                {!! Form::number('price',null,['class' => 'form-control', 'placeholder' => 'Enter price','step' => ".01",]) !!}
                                @component('admin.common.error_helper_text',['key' => "price"])@endcomponent
                            </fieldset>
                            <fieldset class="form-group">
                                <label for="basicInput">Contact Number</label>
                                {!! Form::text('contact_number',null,['class' => 'form-control', 'placeholder' => 'Enter contact number (seperate by comma. Ex: 0xx-xxxx,0xx-xxx)']) !!}
                                @component('admin.common.error_helper_text',['key' => "contact_number"])@endcomponent
                            </fieldset>
                            <fieldset class="form-group">
                                <label for="basicInput">Rating</label>
                                {!! Form::number('rating',null,['class' => 'form-control', 'placeholder' => 'Enter rating ( 1 - 5 )']) !!}
                                @component('admin.common.error_helper_text',['key' => "rating"])@endcomponent
                            </fieldset>
                            <fieldset class="form-group">
                                <label for="basicInput">Youtube Video Id</label>
                                {!! Form::text('youtube_id',null,['class' => 'form-control', 'placeholder' => 'Enter youtube id']) !!}
                                @component('admin.common.error_helper_text',['key' => "youtube_id"])@endcomponent
                            </fieldset>
                            @if(!isset($product))
                                <fieldset>
                                    <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" checked="" name="push_notification" id="customCheck2">
                                    <label class="custom-control-label" for="customCheck2">Push Notification</label>
                                    </div>
                                </fieldset>
                            @endif
                            
                        </div>
                    </div>
                </div>
                @if(isset($product))
                    @can('modify-product')
                    <div class="card-footer">
                        <a href="{{route('admin.product.index')}}" class="btn grey btn-outline-secondary">Discard</a>
                        <button type="submit" class="btn btn-outline-primary">Save changes</button>
                    </div>
                    @endcan
                @else 
                    @can('add-new-product')
                    <div class="card-footer">
                        <a href="{{route('admin.product.store')}}" class="btn grey btn-outline-secondary">Discard</a>
                        <button type="submit" class="btn btn-outline-primary">Save changes</button>
                    </div>
                    @endcan
                @endif
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
@endsection
@section('page-script')
<script>
    $(document).ready( () => {
        var hasPromotion = $('#has-promotion').val()

        if(hasPromotion){
            $('#div-promotion').show()
        }else{
            $('#txt_promotion_price').val('')
            $('#div-promotion').hide()
        }

        $('#btn-promotion').click(() => {
            hasPromotion = !hasPromotion
            if(hasPromotion){
                $('#div-promotion').show()
            }else{
                $('#txt_promotion_price').val('')
                $('#div-promotion').hide()
            }
        })
    })
</script>
@endsection


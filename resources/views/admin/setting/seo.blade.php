{{ Form::open(['route' => ['admin.settings.store'], 'method' => 'POST']) }}
@csrf
<div class="row">
    <div class="col-md-6">
        <fieldset class="form-group">
            <label for="basicInput">SEO Keyword</label>
            {!! Form::text('seo_keyword',isset($settings,$settings['seo_keyword']) ? $settings['seo_keyword']->value : null,['class' => 'form-control', 'placeholder' => 'Enter seo keyword']) !!}
            @component('admin.common.error_helper_text',['key' => "seo_keyword"])@endcomponent
        </fieldset>
        <fieldset class="form-group">
            <label for="basicInput">SEO Description</label>
            {!! Form::text('seo_description',isset($settings,$settings['seo_description']) ? $settings['seo_description']->value : null,['class' => 'form-control', 'placeholder' => 'Enter seo description']) !!}
            @component('admin.common.error_helper_text',['key' => "seo_description"])@endcomponent
        </fieldset>
    </div>
    <div class="col-12">
        <button type="submit" class="btn btn-outline-primary">Save changes</button>
    </div>
</div>
{{ Form::close() }}


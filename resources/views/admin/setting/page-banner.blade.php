{{ Form::open(['route' => ['admin.settings.store'], 'method' => 'POST']) }}
@csrf
<div class="row">
    <div class="col-md-12">
        <fieldset class="form-group">
            <label>About Us Banner (Aspec Ratio 1:8.22)</label>
            @component('admin.common.single_dropzone',['id' => "about_us_banner",'object' => isset($settings,$settings['about_us_banner']) ? $settings['about_us_banner'] : null, 'width' => 822, 'height' => 100])@endcomponent
        </fieldset>
        <fieldset class="form-group">
            <label>Register Banner (Aspec Ratio 1:8.22)</label>
            @component('admin.common.single_dropzone',['id' => "register_banner",'object' => isset($settings,$settings['register_banner']) ? $settings['register_banner'] : null, 'width' => 822, 'height' => 100])@endcomponent
        </fieldset>
        <fieldset class="form-group">
            <label>Event Banner (Aspec Ratio 1:8.22)</label>
            @component('admin.common.single_dropzone',['id' => "event_banner",'object' => isset($settings,$settings['event_banner']) ? $settings['event_banner'] : null, 'width' => 822, 'height' => 100])@endcomponent
        </fieldset>
        <fieldset class="form-group">
            <label>Gallery Banner (Aspec Ratio 1:8.22)</label>
            @component('admin.common.single_dropzone',['id' => "gallery_banner",'object' => isset($settings,$settings['gallery_banner']) ? $settings['gallery_banner'] : null, 'width' => 822, 'height' => 100])@endcomponent
        </fieldset>
        <fieldset class="form-group">
            <label>Member Banner (Aspec Ratio 7:3)</label>
            @component('admin.common.single_dropzone',['id' => "member_banner",'object' => isset($settings,$settings['member_banner']) ? $settings['member_banner'] : null, 'width' => 350, 'height' => 150])@endcomponent
        </fieldset>
        <fieldset class="form-group">
            <label>Footer Banner (Aspec Ratio 5:1)</label>
            @component('admin.common.single_dropzone',['id' => "footer_banner",'object' => isset($settings,$settings['footer_banner']) ? $settings['footer_banner'] : null, 'width' => 700, 'height' => 140])@endcomponent
        </fieldset>
    </div>
    <div class="col-12">
        <button type="submit" class="btn btn-outline-primary">Save changes</button>
    </div>
</div>
{{ Form::close() }}


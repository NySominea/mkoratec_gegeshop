{{ Form::open(['route' => ['admin.settings.store'], 'method' => 'POST']) }}
@csrf
<div class="row">
    <div class="col-md-6">
        <fieldset class="form-group">
            <label>Logo (Aspec Ratio 1:1)</label>
            @component('admin.common.single_dropzone',['id' => "logo",'object' => isset($settings,$settings['logo']) ? $settings['logo'] : null, 'width' => 150, 'height' => 150])@endcomponent
        </fieldset>
        <fieldset class="form-group">
            <label>Animated Logo (Aspec Ratio 2:1)</label>
            @component('admin.common.single_dropzone',['id' => "animated_logo",'object' => isset($settings,$settings['animated_logo']) ? $settings['animated_logo'] : null, 'width' => 200, 'height' => 110])@endcomponent
        </fieldset>
    </div>
    <div class="col-md-6">
        <fieldset class="form-group">
            <label for="basicInput">Site Name</label>
            {!! Form::text('site_name',isset($settings,$settings['site_name']) ? $settings['site_name']->value : null,['class' => 'form-control', 'placeholder' => 'Enter site name']) !!}
            @component('admin.common.error_helper_text',['key' => "site_name"])@endcomponent
        </fieldset>
        <fieldset class="form-group">
            <label for="basicInput">Site Url</label>
            {!! Form::text('site_url',isset($settings,$settings['site_url']) ? $settings['site_url']->value : null,['class' => 'form-control', 'placeholder' => 'Enter site url']) !!}
            @component('admin.common.error_helper_text',['key' => "site_url"])@endcomponent
        </fieldset>
        <fieldset class="form-group">
            <label for="basicInput">Address</label>
            {!! Form::textarea('address',isset($settings,$settings['address']) ? $settings['address']->value : null,['class' => 'form-control', 'placeholder' => 'Enter address','rows' => 3]) !!}
            @component('admin.common.error_helper_text',['key' => "address"])@endcomponent
        </fieldset>
        <fieldset class="form-group">
            <label for="basicInput">Phone</label>
            {!! Form::text('phone',isset($settings,$settings['phone']) ? $settings['phone']->value : null,['class' => 'form-control', 'placeholder' => 'Enter phone number']) !!}
            @component('admin.common.error_helper_text',['key' => "phone"])@endcomponent
        </fieldset>
        <fieldset class="form-group">
            <label for="basicInput">Email</label>
            {!! Form::text('email',isset($settings,$settings['email']) ? $settings['email']->value : null,['class' => 'form-control', 'placeholder' => 'Enter email address']) !!}
            @component('admin.common.error_helper_text',['key' => "email"])@endcomponent
        </fieldset>
        <fieldset class="form-group">
            <label for="basicInput">@ Copy Right</label>
            {!! Form::text('copy_right',isset($settings,$settings['copy_right']) ? $settings['copy_right']->value : null,['class' => 'form-control', 'placeholder' => 'Enter copy right']) !!}
            @component('admin.common.error_helper_text',['key' => "copy_right"])@endcomponent
        </fieldset>
    </div>
    <div class="col-12">
        <button type="submit" class="btn btn-outline-primary">Save changes</button>
    </div>
</div>
{{ Form::close() }}


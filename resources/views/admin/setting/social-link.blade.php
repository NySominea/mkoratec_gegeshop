{{ Form::open(['route' => ['admin.settings.store'], 'method' => 'POST']) }}
@csrf
<div class="row">
    <div class="col-md-6">
        <fieldset class="form-group">
            <label for="basicInput">Facebook</label>
            {!! Form::text('facebook',isset($settings,$settings['facebook']) ? $settings['facebook']->value : null,['class' => 'form-control', 'placeholder' => 'Enter facebook link']) !!}
            @component('admin.common.error_helper_text',['key' => "facebook"])@endcomponent
        </fieldset>
        <fieldset class="form-group">
            <label for="basicInput">Linked In</label>
            {!! Form::text('linkedin',isset($settings,$settings['linkedin']) ? $settings['linkedin']->value : null,['class' => 'form-control', 'placeholder' => 'Enter linkedin link']) !!}
            @component('admin.common.error_helper_text',['key' => "linkedin"])@endcomponent
        </fieldset>
    </div>
    <div class="col-md-6">
        <fieldset class="form-group">
            <label for="basicInput">Instagram</label>
            {!! Form::text('instagram',isset($settings,$settings['instagram']) ? $settings['instagram']->value : null,['class' => 'form-control', 'placeholder' => 'Enter instagram link']) !!}
            @component('admin.common.error_helper_text',['key' => "instagram"])@endcomponent
        </fieldset>
        <fieldset class="form-group">
            <label for="basicInput">Youtube</label>
            {!! Form::text('youtube',isset($settings,$settings['youtube']) ? $settings['youtube']->value : null,['class' => 'form-control', 'placeholder' => 'Enter youtube']) !!}
            @component('admin.common.error_helper_text',['key' => "youtube"])@endcomponent
        </fieldset>
    </div>
    <div class="col-12">
        <button type="submit" class="btn btn-outline-primary">Save changes</button>
    </div>
</div>
{{ Form::close() }}


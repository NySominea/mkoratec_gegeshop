
{{ Form::open(['route' => ['admin.settings.store'], 'method' => 'POST']) }}
@csrf
<div class="row">
    <div class="col-lg-6">
        <fieldset class="form-group">
            <label for="basicInput">Our Story Description</label>
            {!! Form::textarea('our_story_description',isset($settings,$settings['our_story_description']) ? $settings['our_story_description']->value : null,['class' => 'form-control summernote', 'placeholder' => 'Enter our story description','rows' => '4']) !!}
            @component('admin.common.error_helper_text',['key' => "our_story_description"])@endcomponent
        </fieldset>
    </div>
    <div class="col-lg-6">
        <fieldset class="form-group">
            <label for="basicInput">Register Description </label>
            {!! Form::textarea('register_description',isset($settings,$settings['register_description']) ? $settings['register_description']->value : null,['class' => 'form-control summernote', 'placeholder' => 'Enter register description','rows' => '4']) !!}
            @component('admin.common.error_helper_text',['key' => "register_description"])@endcomponent
        </fieldset>
    </div>
    <div class="col-12">
        <button type="submit" class="btn btn-outline-primary">Save changes</button>
    </div>
</div>
{{ Form::close() }}

@section('custom-js')
<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.js"></script>
<script src="{{asset('app-assets/custom/js/summernote.js')}}"></script>
@endsection


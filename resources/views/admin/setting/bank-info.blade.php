{{ Form::open(['route' => ['admin.settings.store'], 'method' => 'POST']) }}
@csrf
<div class="row">
    <div class="col-md-6">
        <fieldset class="form-group">
            <label for="basicInput">Bank Account Name</label>
            {!! Form::text('bank_account_name',isset($settings,$settings['bank_account_name']) ? $settings['bank_account_name']->value : null,['class' => 'form-control', 'placeholder' => 'Enter bank account name']) !!}
            @component('admin.common.error_helper_text',['key' => "bank_account_name"])@endcomponent
        </fieldset>
        <fieldset class="form-group">
            <label for="basicInput">Bank Account Number</label>
            {!! Form::text('bank_account_number',isset($settings,$settings['bank_account_number']) ? $settings['bank_account_number']->value : null,['class' => 'form-control', 'placeholder' => 'Enter bank account number']) !!}
            @component('admin.common.error_helper_text',['key' => "bank_account_number"])@endcomponent
        </fieldset>
    </div>
    <div class="col-12">
        <button type="submit" class="btn btn-outline-primary">Save changes</button>
    </div>
</div>
{{ Form::close() }}


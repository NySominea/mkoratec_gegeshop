<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    @include('admin.layouts.head')
    @yield('page-style')
    @yield('dropzone-css')
</head>
<body class="vertical-layout vertical-menu 2-columns   fixed-navbar" data-open="click" data-menu="vertical-menu" data-col="2-columns">
    @include('admin.layouts.header')
    @include('admin.layouts.sidebar')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                @yield('content')
            </div>
        </div>
    </div>
    <div class="sidenav-overlay"></div>
    <div class="drag-target"></div>
    <footer class="footer footer-static footer-dark navbar-border">
        <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2"><span class="float-md-left d-block d-md-inline-block">Copyright &copy; {{date('Y')}} <a href="/" class="text-bold-800 grey darken-2" target="_blank">On-Sale Cambodia </a></span><span class="float-md-right d-none d-lg-block">Powered By <a class="text-bold-800 grey darken-2" href="https://www.mkoratec.com" target="_blank">MKORATEC </a></span></p>
    </footer>
    @include('admin.layouts.script')
    @yield('dropzone-script')
    @yield('page-script')
</body>
</html>
<div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">

            @canany(['view-product'])
            <li class="nav-item"><a href="{{ route('admin.product.index') }}"><i class="ft-settings"></i><span class="menu-title" data-i18n="">Products</span></a>
            </li>
            @endcanany
            @canany(['view-product-category'])
            <li class="nav-item"><a href="{{ route('admin.product-category.index') }}"><i class="ft-settings"></i><span class="menu-title" data-i18n="">Product Category</span></a>
            </li>
            @endcanany
            @canany(['view-setting','setting-modification'])
            <li class="nav-item"><a href="{{ route('admin.settings.index') }}"><i class="ft-settings"></i><span class="menu-title" data-i18n="">Company Setting</span></a>
            </li>
            @endcanany

            {{-- @canany(['view-language','language-modification'])
            <li class=" nav-item"><a href="{{ route('admin.languages.index') }}"><i class="ft-globe"></i><span class="menu-title" data-i18n="">Languages</span></a>
            </li>
            @endcanany --}}

            @canany(['view-user','user-modification','add-new-user','view-role','role-modification','add-new-role'])
            <li class="nav-item has-sub"><a href="#"><i class="ft-unlock"></i><span class="menu-title" data-i18n="">Administrators</span></a>
                <ul class="menu-content" style="">
                    @canany(['view-user','user-modification','add-new-user'])
                    <li class=""><a class="menu-item" href="{{ route('admin.users.index') }}">Users</a>
                    </li>
                    @endcanany

                    @canany(['view-role','role-modification','add-new-role'])
                    <li class=""><a class="menu-item" href="{{ route('admin.roles.index') }}">Roles</a>
                    </li>
                    @endcanany
                </ul>
            </li>
            @endcanany
        </ul>
    </div>
</div>
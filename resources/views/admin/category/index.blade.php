@extends('admin.layouts.master')
@section('content')
@include(
    'admin.common.success_alert',['key' => 'success']
)
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Product Categories</h4>
                <div class="heading-elements">
                    @can('add-new-category')
                    <a href="{{ route('admin.product-category.create') }}" class="btn btn-sm btn-icon btn-success">
                        <i class="ft-plus white"></i> Add New
                    </a>
                    @endcan
                </div>
            </div>
            <div class="card-content">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead class="bg-primary">
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Description</th>
                                    <th>Image</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody id="sortable">
                                @forelse($categories as $key => $category)
                                    <tr data-id={{ $category->id }}>
                                        <td> <span class="handle">::</span></td>
                                        <td>{{ $category->getTranslation('name','en') }}</td>
                                        <td>{{ $category->getTranslation('description','en') }}</td>
                                        <td>
                                            <img src="{{ $category->getFirstMedia('image')->getUrl() }}"
                                                class="rounded" width="30" height="30"/>
                                        </td>
                                        <td>
                                            @canany(['modify-category'])
                                                <div class="btn-group btn-group-sm" aria-label="Basic example">
                                                    <a href="{{ route('admin.product-category.edit',$category->id) }}" class="btn btn-outline-warning"><i class="ft-edit-3"></i> Edit</a>
                                                    <button type="button" class="btn btn-outline-danger delete" data-route="{{route('admin.product-category.destroy',$category->id)}}"><i class="ft-trash-2"></i> Delete</button>
                                                </div>
                                            @endcanany
                                        </td>
                                    </tr>
                                @empty
                                    <tr><td colspan="3">No Data</td></tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page-script')
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>

<script>
    $(document).ready(function(){
        if($('#sortable').length > 0){

            $("#sortable td").each(function () {
                $(this).css("width", $(this).width());
            });

            $('#sortable').sortable({
                scroll: false,
                handle: '.handle',
                update: function (event, ui) {
                    var order_id = [];

                    $('.overlay').show();
                    $("#sortable tr").each(function(index) {
                        order_id.push($(this).data('id'));
                    });
                    $.ajax({
                        type:'POST',
                        url:'/admin/set-order-category',
                        data: {order_id:order_id},
                        success:function(data){

                            if(data.success){
                                swal({
                                    title: 'Done',
                                    text: "Updated Successfully!",
                                    icon: 'success',
                                    timer: 1000,
                                });
                            }
                        },
                        error: function(){
                            swal({
                                title: 'Error',
                                text: "Error While Updating!",
                                icon: 'error',
                                timer: 1000,
                            });
                        }
                    });


                }
            });
            $( "#sortable" ).disableSelection();
        }

    })
</script>
@endsection

@extends('admin.layouts.master')

@section('page-css')
<link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/vendors.min.css">
<link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/forms/icheck/icheck.css">
<link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/forms/icheck/custom.css">
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">{{ isset($category) ? 'Update' : 'Add' }} Product Category</h4>
            </div>

            @if(isset($category))
            {{ Form::model($category,['route' => ['admin.product-category.update',$category->id], 'method' => 'PUT']) }}
            @else
            {{ Form::open(['route' => 'admin.product-category.store', 'method' => 'POST']) }}
            @endif
            @csrf
            <div class="card-content">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <fieldset class="form-group">
                                <label>Product Category Image (Aspec Ratio 1:1) <span class="text-danger">*</span></label>
                                @component('admin.common.single_dropzone',['id' => "categoryDropzone",'object' => isset($category) ? $category : null, 'width' => 150, 'height' => 150,'collection_key' => 'image'])@endcomponent
                            </fieldset>
                        </div>
                        <div class="col-md-6">
                            <ul class="nav nav-tabs nav-top-border no-hover-bg" role="tablist">
                                @foreach(['en','kh','zh'] as $key => $lang)
                                <li class="nav-item">
                                    <a class="nav-link {{ $key ==0 ? 'active' : '' }}" id="base-{{$lang}}" data-toggle="tab" aria-controls="{{$lang}}" href="#{{$lang}}" role="tab" aria-selected="true">
                                        <img width="30px" src="{{asset('client/images/'.$lang.'.jpeg')}}" alt="">
                                    </a>
                                </li>
                                @endforeach
                            </ul>
                            <div class="tab-content px-1 pt-1">
                                @foreach(['en','kh','zh'] as $key => $lang)
                                    <div class="tab-pane {{ $key == 0 ? 'active' : ''}}" id="{{$lang}}" role="tabpanel" aria-labelledby="base-{{$lang}}">
                                        <fieldset class="form-group">
                                            <label for="basicInput">Name <img width="20px" src="{{asset('client/images/'.$lang.'.jpeg')}}" alt=""></label>
                                            {!! Form::text('name['.$lang.']',isset($category) ? $category->getTranslation('name',$lang) : null,['class' => 'form-control', 'placeholder' => 'Enter category name']) !!}
                                            @component('admin.common.error_helper_text',['key' => "name.$lang"])@endcomponent
                                        </fieldset>
                                        <fieldset class="form-group">
                                            <label for="basicInput">Description <img width="20px" src="{{asset('client/images/'.$lang.'.jpeg')}}" alt=""></label>
                                            {!! Form::textarea('description['.$lang.']',isset($category) ? $category->getTranslation('description',$lang) : null,['class' => 'form-control', 'placeholder' => 'Enter category description','rows' => '4']) !!}
                                            @component('admin.common.error_helper_text',['key' => "description"])@endcomponent
                                        </fieldset>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                @if(isset($category))
                    @can('modify-category')
                    <div class="card-footer">
                        <a href="{{route('admin.product-category.index')}}" class="btn grey btn-outline-secondary">Discard</a>
                        <button type="submit" class="btn btn-outline-primary">Save changes</button>
                    </div>
                    @endcan
                @else
                    @can('add-new-category')
                    <div class="card-footer">
                        <a href="{{route('admin.product-category.store')}}" class="btn grey btn-outline-secondary">Discard</a>
                        <button type="submit" class="btn btn-outline-primary">Save changes</button>
                    </div>
                    @endcan
                @endif
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
@endsection

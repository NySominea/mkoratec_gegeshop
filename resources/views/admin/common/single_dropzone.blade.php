
@section('dropzone-css')
<link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/file-uploaders/dropzone.min.css">
<link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/ui/prism.min.css">
<link rel="stylesheet" type="text/css" href="/app-assets/css/plugins/file-uploaders/dropzone.css">
@endsection

<style>
    #{{$id}} .dz-preview .dz-image img{
        width:100%;
    }
    #{{$id}} .dz-preview .dz-image{
        width: @php echo $width.'px' @endphp !important;
        height: @php echo $height.'px' @endphp !important;
    }
</style>

@php 
    $collection_key = isset($collection_key) ? $collection_key : 'images'; 
    $image = isset($object) ? $object->getMedia($collection_key)->first() : null; 
    $imageId = $id.'Image';
    $actionId = $id.'Action';
@endphp

<div class="dropzone needsclick dz-clickable" action="{{ route('admin.dropzoneSaveTempImage') }}" id="{{$id}}"
    data-width="{{$width}}" data-height="{{$height}}" data-input="{{$imageId}}" data-action="{{$actionId}}">
    @csrf
</div>
@component('admin.common.error_helper_text',['key' => $imageId])@endcomponent

@if($image)
    <input type="hidden" name="{{$imageId}}" id="{{$imageId}}" value="{{old('image')}}" data-model-id="{{$object->id}}" data-name="{{$image->file_name}}" data-size="{{$image->size}}" data-url="{{ asset($image->getUrl()) }}">
@else
    <input type="hidden" name="{{$imageId}}" id="{{$imageId}}" value="">
@endif
<input type="hidden" name="{{$actionId}}" id="{{$actionId}}" value="{{isset($object) ? 'update' : 'add'}}">


@section('dropzone-script')
<script src="/app-assets/vendors/js/extensions/dropzone.min.js"></script>
<script src="/app-assets/vendors/js/ui/prism.min.js"></script>
<script src="/app-assets/custom/js/dropzone.js"></script>
@endsection

@section('dropzone-css')
<link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/file-uploaders/dropzone.min.css">
<link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/ui/prism.min.css">
<link rel="stylesheet" type="text/css" href="/app-assets/css/plugins/file-uploaders/dropzone.css">
@endsection

<style>
    .dropzone .dz-preview .dz-image img{
        width:100%;
    }
    .dropzone .dz-preview .dz-image{
        width: @php echo $width.'px' @endphp !important;
        height: @php echo $height.'px' @endphp !important;
    }
</style>

@php 
    $collection_key = isset($collection_key) ? $collection_key : 'album'; 
    $imageId = $id.'Image';
    $actionId = $id.'Action';
    $deleteId = $id.'Delete';
@endphp

<div class="dropzone multi needsclick dz-clickable" action="{{ route('admin.dropzoneSaveMultiTempImage') }}" id="{{$id}}"
    data-width="{{$width}}" data-height="{{$height}}" data-input="{{$imageId}}" data-action="{{$actionId}}" data-delete="{{$deleteId}}"
    data-route-get-image="{{ route('admin.getImageList') }}" data-route-delete-image="{{ route('admin.deleteDropzoneImage') }}"
    data-model="{{isset($object) ? get_class($object) : ''}}" data-collection={{$collection_key}}>
    @csrf
</div>
@component('admin.common.error_helper_text',['key' => $imageId])@endcomponent

@if($object)
    <input type="hidden" name="{{$imageId}}" id="{{$imageId}}" value="{{old('image')}}" data-model-id="{{$object->id}}">
@else
    <input type="hidden" name="{{$imageId}}" id="{{$imageId}}" value="">
@endif
<input type="hidden" name="{{$actionId}}" id="{{$actionId}}" value="{{isset($object) ? 'update' : 'add'}}">
<input type="hidden" name="{{$deleteId}}" id="{{$deleteId}}" value="">


@section('dropzone-script')
<script src="/app-assets/vendors/js/extensions/dropzone.min.js"></script>
<script src="/app-assets/vendors/js/ui/prism.min.js"></script>
<script src="/app-assets/custom/js/dropzone.js"></script>
@endsection
<?php

Route::get('/',function(){
    return redirect()->route('login');
});

Route::namespace('Admin')->group(function(){
    Route::namespace('Auth')->group(function(){
        Route::get('/login', ['uses' => 'LoginController@showLoginForm', 'as' => 'login']);
        Route::post('/login', ['uses' => 'LoginController@login', 'as' => 'postLogin']);
        Route::post('/logout', ['uses' => 'LoginController@logout', 'as' => 'logout']);
    });

    Route::group(['middleware' => 'auth','prefix' => 'admin', 'as' => 'admin.'], function(){
        Route::get('/dashboard',['uses' => 'DashboardController@index', 'as' => 'dashboard']);

        Route::group(['middleware' => ['permission:view-product|modify-product']],function(){
            Route::resource('product','ProductController');
        });

        Route::group(['middleware' => ['permission:view-category|modify-category']],function(){
            Route::resource('product-category','ProductCategoryController');
        });

        Route::group(['middleware' => ['permission:view-setting|setting-modification']], function () {
            Route::resource('settings','SettingController');
        });
        Route::group(['middleware' => ['permission:view-language|language-modification']], function () {
            Route::resource('languages','LanguageController');
        });
        Route::group(['middleware' => ['permission:view-user|user-modification|add-new-user']], function () {
            Route::resource('/users','UserController');
        });
        Route::group(['middleware' => ['permission:view-role|role-modification|add-new-role']], function () {
            Route::resource('/roles','UserRoleController');
        });
        Route::resource('/users/profile','EditProfileController',['as' => 'users']);

        //ajax upload image
        Route::post('/save-temp-image', ['as' => 'dropzoneSaveTempImage', 'uses' => 'ImageController@dropzoneSaveTempImage']);
        Route::post('/save-multi-temp-image', ['as' => 'dropzoneSaveMultiTempImage', 'uses' => 'ImageController@dropzoneSaveMultiTempImage']);
        Route::post('/save-summernote-image',['as' => 'summernoteSaveImage', 'uses' => 'ImageController@summernoteSaveImage']);
        Route::get('/get-image-list-by-object',['as' => 'getImageList', 'uses' => 'ImageController@getImageList']);
        Route::post('/delete-image-by-object',['as' => 'deleteDropzoneImage', 'uses' => 'ImageController@deleteDropzoneImage']);
        Route::post('/set-order-category',['as' => 'setOrderCategory', 'uses' => 'ProductCategoryController@setOrder']);
    });
});

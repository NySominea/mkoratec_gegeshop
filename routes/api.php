<?php

use Illuminate\Http\Request;

Route::namespace('Api')->group(function(){
    Route::get('home','HomeController@home');

    Route::get('special-offers','HomeController@specialOffers');

    Route::get('search','HomeController@searchProducts');

    Route::get('categories','HomeController@getCategories');

    Route::get('get-products-by-category','HomeController@getProductsByCategory');

    Route::get('get-special-offer-products','HomeController@getSpecialOfferProducts');

    Route::get('get-popular-products','HomeController@getPopularProducts');
});


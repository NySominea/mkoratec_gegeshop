
// NAVBAR MENU
$(document).ready(function(){
    let windowScroll = $(window).scrollTop()
    let nav = $("#navbar");

    // navbar trasnformation on scroll
    changeNavStyleOnScroll(windowScroll);
    $(window).scroll(function() {
      windowScroll = $(this).scrollTop();
      changeNavStyleOnScroll(windowScroll);
    });

    function changeNavStyleOnScroll(){
        if(windowScroll >= nav.height()) {
            nav.addClass('sticky-top bg-white shadow animated fadeInDown');
        }else{
            windowScroll = 0;
            nav.removeClass('sticky-top shadow animated fadeInDown' )
        }
        $('.navbar-collapse').collapse('hide');
    }

    $(".nav-link.active").parents('.nav-item').addClass('active');

    $('.nav-item.dropdown').on('click', function() {
        location.href = $(this).find('.nav-link').attr('href');
    });
})

// Check Navbar Active
$(document).ready(function(){
    var current = (location.origin).concat(location.pathname).concat(location.hash).replace(/\/$/, '');
    
    $('.nav-item a').each(function(){
        if ($(this).attr('href').replace(/\/$/, '') == current) {
            $(this).addClass('active');
            $(this).parents('.nav-item').addClass('active');
        }
    })
})
// END NAVBAR MENU

// OWL CAROUSEL
$('#homepage .partner .owl-carousel').owlCarousel({
    loop: true,
    margin: 40,
    autoplay: true,
    autoplayTimeout: 3000,
    slideTransition: 'linear',
    smartSpeed:3000,
    // autoplayHoverPause: true,
    dots:false,
    navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
    responsive:{
        0:{
            items:3,
            loop:$('.owl-carousel').children().length > 3 ? true : false, 
            margin: $('.owl-carousel').children().length > 2 ? 30 : $('.owl-carousel').children().length == 1 ? 0 : 15
        },
        420:{
            items:4,
            loop:$('.owl-carousel').children().length > 4 ? true : false, 
            margin: $('.owl-carousel').children().length > 3 ? 30 : $('.owl-carousel').children().length == 1 ? 0 : 15
        },
        991:{
            items:5,
            loop:$('.owl-carousel').children().length > 5 ? true : false, 
            margin: $('.owl-carousel').children().length > 4 ? 50 : $('.owl-carousel').children().length == 1 ? 0 : 30
        },
        1200:{
            items:6,
            loop:$('.owl-carousel').children().length > 6 ? true : false, 
            margin: $('.owl-carousel').children().length > 5 ? 50 : $('.owl-carousel').children().length == 1 ? 0 : 30
        }
    }
});

//====Scroll UP button ====
$(document).ready(function(){
    btn = $("#btnScrollUp");

    btn.click(function(){
        $('html,body').animate({
            'scrollTop' : 0,
        },500)
    });
    $(window).on('scroll',function(){
        if($(this).scrollTop() > 100){
            if(!btn.is(':visible')){
                btn.fadeIn('slow');
            }
        }else{
            btn.fadeOut();
        }
    })
});


// Image Gallery
$(document).on("click", '[data-toggle="lightbox"]', function(event) {
    event.preventDefault();
    $(this).ekkoLightbox();
});



